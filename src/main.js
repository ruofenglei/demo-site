import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import Buefy from "buefy";
import "buefy/lib/buefy.css";

import { library } from "@fortawesome/fontawesome-svg-core";
import {
    faCircle,
    faPlusCircle,
    faMinusCircle
} from "@fortawesome/free-solid-svg-icons";
import { far } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(faCircle, faPlusCircle, faMinusCircle, far);
Vue.component("font-awesome-icon", FontAwesomeIcon);

import YellowButton from "./components/YellowButton.vue";
import WhiteButton from "./components/WhiteButton.vue";
import MyCard from "./components/MyCard.vue";

Vue.component("YellowButton", YellowButton);
Vue.component("WhiteButton", WhiteButton);
Vue.component("MyCard", MyCard);
Vue.use(Buefy);

Vue.config.productionTip = false;

new Vue({
    router,
    render: h => h(App)
}).$mount("#app");
